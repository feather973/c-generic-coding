#pragma warning(disable:4996) // C4996 에러를 무시
#include <stdio.h>
void *find(void *begin, void *end, void *value, int (*compareData)(void *, void *), void (*nextData)(void *) );
void nextInt(void *vp);
int compareInt(void *vp1, void *vp2);
void nextDouble(void *vp);
int compareDouble(void *vp1, void *vp2);

int main()
{
	int x[10] = { 1,2,3,4,5,6,7,8,9,10 };
	double  y[5] = { 1.1, 2.2, 3.3, 4.4, 5.5 };
	int number=5;
	double dNumber = 5.5;
	int *ip;
	double *dp;

	ip = (int *)find( x, x + 10, &number, compareInt, nextInt);

	if ( ip == x + 10 ){ printf("not find\n"); }
	else{ printf("find data = %d\n", *ip); }

	dp = (double *)find( y, y+5, &dNumber, compareDouble,  nextDouble );
	if ( dp == y + 5){ printf("not find\n"); }
	else{ printf("find data = %lf\n", *dp); }

	dNumber = 7.7;  // 존재하지 않는 데이터 검색 테스트  
	dp = (double *)find( y, y+5, &dNumber, compareDouble,  nextDouble );
	if ( dp == y + 5){ printf("not find\n"); }
	else{ printf("find data = %lf\n", *dp); }	
	
	return 0;
}

/*-------------------------------------------------------------------------
  함수명 : find
  전달인자 : begin - 첫번째 데이터의 주소
        end - 마지막 데이터의 주소
        value - 검색할 데이터의 주소
        compareData - 데이터를 비교하는 보조함수의 시작주소
        nextData - 다음 데이터로 이동하는 함수의 시작주소
  리턴값 : 찾은 데이터의 시작주소  또는 못 찾을 경우 end 값
 ------------------------------------------------------------------------*/
void* find(void* start, void* end, void* value, int (*compareData)(void*, void*), void (*nextData)(void*))
{
	while (start != end) {
		if (compareData(start, value)) {
			break;
		}
		nextData(&start);		// void **
		printf("start : %p\n", start);
	}
	return start;
}
/*-------------------------------------------------------------------------
  함수명 : compareInt
  전달인자 : vp1, vp2 - 비교할 두 데이터의 시작주소
  리턴값 : 두 데이터값이 같으면 1,  다르면 0 리턴
 ------------------------------------------------------------------------*/
int compareInt(void* vp1, void* vp2)
{
	int* ip1 = vp1;
	int* ip2 = vp2;

	if (*ip1 == *ip2) {
		return 1;
	}
	else {
		return 0;
	}
}

/*-------------------------------------------------------------------------
  함수명 : nextInt
  전달인자 : vp - 현제 데이터의 위치를 가리키는 포인터 변수를 가리키는 포인터
  리턴값 : 없음
  void ** 도 가능, 하지만 불필요
 ------------------------------------------------------------------------*/
void nextInt(void* vp)		// 자료형 크기 4bytes
{
	int** ipp = vp;
	*ipp += 1;				// 포인터 위치이동
}

/*-------------------------------------------------------------------------
  함수명 : compareDouble
  전달인자 : vp1, vp2 - 비교할 두 데이터의 시작주소
  리턴값 : 두 데이터값이 같으면 1,  다르면 0 리턴
 ------------------------------------------------------------------------*/
int compareDouble(void* vp1, void* vp2)
{
	double* dp1 = vp1;
	double* dp2 = vp2;

	if (*dp1 == *dp2) {
		return 1;
	}
	else {
		return 0;
	}
}

/*-------------------------------------------------------------------------
  함수명 : nextDouble
  전달인자 : vp - 현제 데이터의 위치를 가리키는 포인터 변수를 가리키는 포인터
  리턴값 : 없음
  void ** 도 가능, 하지만 불필요
 ------------------------------------------------------------------------*/
void nextDouble(void* vp)
{
	double** dpp = vp;
	*dpp += 1;
}