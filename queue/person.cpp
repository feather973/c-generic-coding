#pragma warning(disable:4996) // C4996 에러를 무시

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "person.h"

// 깊은 복사 필요 (addr)
void personMemCpy(void* p1, void* p2)
{
	Person* ps1 = (Person *) p1;
	Person* ps2 = (Person *) p2;

	strcpy(ps1->name, ps2->name);
	if(ps1->addr != NULL){		// double free 방지코드
		free(ps1->addr);
	}
	ps1->addr = (char*) calloc(1, strlen(ps2->addr) + 1);
	if (ps1->addr == NULL) {
		printf("personMemCpy 복사실패\n");
		return;
	}
	strcpy(ps1->addr, ps2->addr);
	ps1->age = ps2->age;
}

//addr에 할당된 부가 메모리 해제 및 멤버 초기화 
void personClear(void* p)
{
	Person* ps = (Person*)p;

	if (ps->addr != NULL) {
		free(ps->addr);				// free 수행 ( personMemCpy 에서 double free 검사 )
		ps->addr = NULL;
	}

	strcpy(ps->name, "");			//	memset(ps->name, 0, sizeof(ps->name));
	ps->age = 0;	
}

void personPrint(void* p)
{
	Person* ps = (Person*)p;

	printf("%s / %s / %d\n",
		ps->name, ps->addr, ps->age);
}