#pragma once
typedef struct _person{
	char name[20];
	char *addr;  // 주소저장을 위한 동적메모리 할당이 예정 
	int age;
}Person;

void personMemCpy(void *p1, void *p2);
void personClear(void *p);  //addr에 할당된 부가 메모리 해제 및 멤버 초기화  
void personPrint(void *p);
