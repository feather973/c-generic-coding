#include "circularQueue.h"
#include <stdlib.h>

/* 큐 생성 및 초기화 함수 */
BOOL createQueue(Queue* qp, int size, size_t dataSize)
{
    if (qp == NULL) {
        return FALSE;
    }
    qp->queue = calloc(1, size * dataSize);
    if (qp->queue == NULL) {
        printf("queue 할당 실패\n");
        return FALSE;
    }
    qp->size = size;
    qp->front = 0;
    qp->rear = 0;
    return TRUE;
}

/* 큐가 완전히 비어있는가 */
BOOL isQueueEmpty(const Queue* qp)
{
    if (qp == NULL) {
        return FALSE;
    }

    if (qp->front == qp->rear) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}
/* 큐가 꽉차있는가 검사 */
BOOL isQueueFull(const Queue* qp)
{
    int next_rear;
    
    if (qp == NULL) {
        return FALSE;
    }

    next_rear = (qp->rear + 1) % qp->size;
    if (next_rear == qp->front) {
        return TRUE;
    }
    else {
        return FALSE;
    }
}
/* 큐에 데이터 하나를 저장 함 */
BOOL enqueue(Queue* qp, void* enqueData, size_t dataSize, void (*memCpy)(void*, void*))
{
    char* cptr;

    if (qp == NULL) {
        return FALSE;
    }

    cptr = (char*)(qp->queue);

    if (isQueueFull(qp)) {
        printf("Queue is Full\n");
        return FALSE;
    }

    memCpy(cptr + dataSize * (qp->rear), enqueData);
    qp->rear = (qp->rear + 1) % qp->size;               // modular
    
    return TRUE;
}
/* 큐에서 데이터 하나를 꺼냄 */
BOOL dequeue(Queue* qp, void* dequeData, size_t dataSize,
    void (*memCpy)(void*, void*), void (*memClear)(void*))
{
    char* cptr;

    if (qp == NULL) {
        return FALSE;
    }

    cptr = (char*)(qp->queue);

    if (isQueueEmpty(qp)) {
        printf("Queue is Empty\n");
        return FALSE;
    }

    memCpy(dequeData, cptr + dataSize * (qp->front));
    memClear(cptr + dataSize * (qp->front));
    qp->front = (qp->front + 1) % qp->size;             // modular

    return TRUE;
}
/* 큐 내의 모든 데이터를 출력(dequeue하는 것은 아님) */
void printQueue(const Queue* qp, size_t dataSize, void (*dataPrint)(void*))
{
    int i;
    char* cptr;
    
    if (qp == NULL) {
        return;
    }

    cptr = (char*)(qp->queue);

    for (i = qp->front; i != qp->rear; i = (i + 1) % qp->size) {
        dataPrint(cptr + dataSize * i);
    }
}
/* 큐 소멸 함수 */
// 원형큐에 붙어있는 개별메모리도 해제필요!
void destroyQueue(Queue* qp, size_t dataSize, void (*memClear)(void*))
{
    char* cptr;
    int i;

    if (qp == NULL) {
        return;
    }

    cptr = (char*)(qp->queue);

    if (qp->queue != NULL) {
        for (i = qp->front; i != qp->rear; i = (i + 1) % qp->size) {
            memClear(cptr + dataSize * i);
        }
        free(qp->queue);
    }
    qp->queue = NULL;
    qp->size = 0;
    qp->front = qp->rear = 0;
}