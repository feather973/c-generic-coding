//#pragma once			// inclusion guard #type1
#ifndef PERSON_H		// inclusion guard #type2
#define PERSON_H
typedef struct _person{
	char name[20];
	int age;
}Person;

void personMemCpy(void *, void *);
void personPrint(void *);
#endif