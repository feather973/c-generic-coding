#include <stdio.h>
#include <string.h>

#include "person.h"

void personMemCpy(void* vp1, void* vp2)
{
	Person* ps1 = (Person*) vp1;
	Person* ps2 = (Person*) vp2;

	// Person 엔 동적메모리가 없다.
	// 따라서 "메모리 해제-길이만큼 재할당" 없이 바로 카피
	ps1->age = ps2->age;
	memcpy(ps1, ps2, sizeof(Person));
}
void personPrint(void* vp)
{
	Person* ps = (Person*)vp;
	
	printf("%s(%d)\n", ps->name, ps->age);
}