#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#include "arrayStack.h"
enum { EMPTY = -1 };

/* 스택 메모리 할당 및 멤버 초기화 함수 */
BOOL createStack(Stack* sPtr, int size, size_t dataSize)
{
	sPtr->stack = calloc(1, size * dataSize);
	if (sPtr->stack == NULL)
	{
		printf("스택 생성실패\n");
		return FALSE;
	}

	sPtr->size = size;
	sPtr->top = EMPTY;		// 스택 push / pop 시 증감
	return TRUE;
}
/* 스택이 꽉 차있는지 검사 */
BOOL isStackFull(Stack* sPtr)
{
	if ((sPtr->size - 1) == (sPtr->top)){
		return TRUE;
	}
	else {
		return FALSE;
	}
}
/* 스택이 완전히 비어있는가 검사 */
BOOL isStackEmpty(Stack* sPtr)
{
	if (sPtr->top == EMPTY) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}
/* 스택에 데이터 저장하는 함수 */
BOOL push(Stack* sPtr, void* inData, size_t dataSize, void (*memCpy)(void*, void*))
{
	char* cptr = (char*)(sPtr->stack);

	if (isStackFull(sPtr)) {
		printf("Stack is Full\n");
		return FALSE;
	}

	// top 증가
	(sPtr->top)++;

	// 데이터 in
	memCpy(cptr + (dataSize * sPtr->top), inData);
	return TRUE;
}
/* 스택에서 데이터를 꺼내는 함수 */
BOOL pop(Stack* sPtr, void* popData, size_t dataSize, void (*memCpy)(void*, void*))
{
	char* cptr = (char*)(sPtr->stack);

	if (isStackEmpty(sPtr)) {
		printf("Stack is Empty\n");
		return FALSE;
	}

	// 데이터 out
	memCpy(popData, cptr + (dataSize * sPtr->top));

	// top 감소
	(sPtr->top)--;
	return TRUE;
}
/* 스택 내의 모든 데이터를 출력하는 함수 */
void printStack(const Stack* sPtr, size_t dataSize, void (*dataPrint)(void*))
{
	char* cptr = (char*)(sPtr->stack);
	int i;

	for (i = 0; i <= sPtr->top; i++) {		// top 이 가리키는 곳에 원소 존재
		dataPrint(cptr + (dataSize)*i);
	}
}
/* 스택 메모리 해제 함수 */
void destroyStack(Stack* sPtr)
{
	// stack 해제
	free(sPtr->stack);
}