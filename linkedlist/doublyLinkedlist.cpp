#include <stdbool.h>
#include "doublyLinkedlist.h"
#include <stdlib.h>
#include <stdio.h>

/* 연결 리스트 초기화 */
BOOL createList(List* lp) {
	Node* head = (Node *)calloc(1, sizeof(Node));
	if (head == NULL) {
		printf("head 노드 할당실패\n");
		return FALSE;
	}
	lp->head = head;

	Node* tail = (Node*)calloc(1, sizeof(Node));
	if (tail == NULL) {
		printf("tail 노드 할당실패\n");
		return FALSE;
	}
	lp->tail = tail;

	lp->head->next = tail;
	lp->head->prev = head;		// 자기자신

	lp->tail->prev = head;
	lp->tail->next = tail;		// 자기자신

	lp->size = 0;
	return TRUE;
}

/* head node 뒤에 node 추가(역순 저장) */
BOOL addFirst(List* lp, void* data, size_t dataSize, void (*memCpy)(void*, void*)) {
	Node* newp = (Node*)calloc(1, sizeof(Node) + dataSize);			// Node 뒤에 data 추가해서 관리
	if (newp == NULL) {
		printf("new 노드 할당실패\n");
		return FALSE;
	}

	memCpy(newp + 1, data);		// Node 뒤에 data 카피

	// 업데이트 순서중요 : 새 노드 -- 새 노드의 다음노드 -- head 노드

	// 1) 새 노드연결 업데이트
	newp->next = lp->head->next;
	newp->prev = lp->head;

	// 2) 새 노드의 다음노드 연결 업데이트 (중요!)
	lp->head->next->prev = newp;

	// 3) head 노드연결 업데이트
	lp->head->next = newp;

	// 리스트 사이즈 증가
	(lp->size)++;
	return TRUE;
}

/* tail node 앞에 node 추가(정순 저장) */
BOOL addLast(List* lp, void* data, size_t dataSize, void (*memCpy)(void*, void*)) {
	// 새 노드 생성
	Node* newp = (Node*)calloc(1, sizeof(Node) + dataSize);			// Node 뒤에 data 추가해서 관리
	if (newp == NULL) {
		printf("new 노드 할당실패\n");
		return FALSE;
	}
	
	memCpy(newp + 1, data);		// Node 뒤에 data 카피
	
	// 업데이트 순서중요 : 새 노드 -- 새 노드의 전노드 -- tail 노드
	
	// 1) 새 노드연결 업데이트
	newp->prev = lp->tail->prev;
	newp->next = lp->tail;

	// 2) 새 노드 전노드 연결 업데이트 (중요!)
	lp->tail->prev->next = newp;			// NULL POINTER (lp->tail->prev)

	// 3) tail 노드연결 업데이트
	lp->tail->prev = newp;

	// 리스트 사이즈 증가
	(lp->size)++;
	return TRUE;
}

/* 리스트 내의 모든 데이터 출력 */
void displayList(List* lp, void (*dataPrint)(void*)) {
	Node* np;
	
	for (np = lp->head->next; np != lp->tail; np = np->next) {
		dataPrint(np + 1);
	}
}

/* data 노드 삭제 */
BOOL removeNode(List* lp, void* data, int (*compare)(void*, void*), void (*clear)(void*)) {
	Node* delp;

	delp = searchNode(lp, data, compare);
	if (delp == NULL) {
		return FALSE;
	}

	// 양옆 노드 연결
	delp->prev->next = delp->next;
	delp->next->prev = delp->prev;

	// Node 뒤에 있던 data 해제
	clear(delp+1);

	// Node 해제
	free(delp);

	// 리스트 사이즈 감소
	(lp->size)--;
	return TRUE;
}

/* data와 일치하는 node 검색 */
Node* searchNode(List* lp, void* data, int (*compare)(void*, void*)) {
	Node* np;

	for (np = lp->head->next; np != lp->tail; np = np->next) {
		if (!compare(np+1, data)) {		// np == data : 0
			return np;
		}
	}

	return NULL;
}

/* 노드 정렬 - 오름차순 */
void sortList(List* lp, size_t dataSize, int (*compare)(void*, void*),
	void (*memCpy)(void*, void*), void (*clear)(void*)) {
	Node* curp;
	Node* nextp;
	
	void * swap_mem;	// 임시공간
	swap_mem = (char*)calloc(1, dataSize);
	if (swap_mem == NULL) {
		printf("정렬용 임시공간 할당실패\n");
		return;
	}

	for (curp = lp->head->next; curp != lp->tail; curp = curp->next) {
		for (nextp = curp->next; nextp != lp->tail; nextp = nextp->next) {
			if (compare(curp+1, nextp+1) == 1) {		// node 가 아닌, node 다음 data 를 비교
				memCpy(swap_mem, nextp+1);
				memCpy(nextp+1, curp+1);
				memCpy(curp+1, swap_mem);
			}
		}
	}
}
/* 리스트 내의 모든 노드를 삭제 */
void destroyList(List* lp, void (*clear)(void*)) {
	// 다음 포인터를 보관해둬야 한다.
	Node* curp;
	Node* nextp;

	curp = lp->head->next;
	
	while (curp != lp->tail) {
		// next 보관
		nextp = curp->next;

		// curp data 삭제
		clear(curp + 1);

		// curp node 해제
		free(curp);

		// curp 업데이트
		curp = nextp;
	}

	free(lp->head);
	free(lp->tail);

	lp->head = NULL;
	lp->tail = NULL;
	lp->size = 0;
}