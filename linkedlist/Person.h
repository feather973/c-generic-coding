#pragma once
typedef struct _person {
	char* name;
	int age;
	char phone[16];
}Person;

void personMemCopy(void* p1, void* p2);  // p2번지의 내용을 p1번지에 깊은복사
void personClear(void* p);  // p번지의 구조체 내용을 모두 삭제
  //(name멤버는 해제하고 NULL pointer로 변경, age는 0으로 변경, phone멤버는 ""으로 초기화)
void personPrint(void* p); // p번지의 구조체 내용을 한줄로 출력(/로 구분)
int personNameCompare(void* p1, void* p2);  // p1->name 이 p2->name 보다 크면 1, 같으면 0, 작으면 -1 리턴
int personAgeCompare(void* p1, void* p2); // p1->age가 p2->age 보다 크면 1, 같으면 0, 작으면 -1 리턴
