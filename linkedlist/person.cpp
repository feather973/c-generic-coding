#pragma warning(disable:4996) // C4996 에러를 무시

#include "Person.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

// p2번지의 내용을 p1번지에 깊은복사
void personMemCopy(void* p1, void* p2) {
	Person* ps1 = (Person * )p1;
	Person* ps2 = (Person * )p2;

	if (ps1->name != NULL) {
		free(ps1->name);
	}
	ps1->name = (char*)calloc(1, strlen(ps2->name) + 1);
	if (ps1->name == NULL) {
		printf("깊은복사 메모리 할당 실패\n");
		return;
	}
	else {
		strcpy(ps1->name, ps2->name);
	}

	ps1->age = ps2->age;

	strcpy(ps1->phone, ps2->phone);
}

// p번지의 구조체 내용을 모두 삭제
//(name멤버는 해제하고 NULL pointer로 변경, age는 0으로 변경, phone멤버는 ""으로 초기화)
void personClear(void* p) {
	Person* ps = (Person*)p;

	ps->age = 0;
	strcpy(ps->phone, "");
	
	if (ps->name != NULL) {
		free(ps->name);
		ps->name = NULL;
	}
}

// p번지의 구조체 내용을 한줄로 출력(/로 구분)
void personPrint(void* p) {
	Person* ps = (Person*)p;

	printf("%s / %d / %s\n", 
		ps->name, ps->age, ps->phone);
}

// p1->name 이 p2->name 보다 크면 1, 같으면 0, 작으면 -1 리턴
int personNameCompare(void* p1, void* p2) {
	Person* ps1 = (Person*)p1;
	Person* ps2 = (Person*)p2;

	return strcmp(ps1->name, ps2->name);
}

// p1->age가 p2->age 보다 크면 1, 같으면 0, 작으면 -1 리턴
int personAgeCompare(void* p1, void* p2) {
	Person* ps1 = (Person*)p1;
	Person* ps2 = (Person*)p2;

	if (ps1->age > ps2->age) {
		return 1;
	}
	else if (ps1->age == ps2->age) {
		return 0;
	}
	else {
		return -1;
	}
}